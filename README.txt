﻿#######################################################################################

									GALLERIET SERVER
						readme on how to set up a new django app
---------------------------------------------------------------------------------------
author: Jakob R Müller
created:	2018-03-05
edit:		2019-01-07	added sourcetree as software and galleriet repo adress

#######################################################################################

server-IP: http://129.16.63.205:81/ 

	
	/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ 
	/!\ 			FIRST: BACKUP THE SERVER!				/!\ 
	/!\ 										/!\ 
	/!\ do a git commit of GallerietServer. You don't have to push it to the remote,/!\  
	/!\ except you're planning to do some weird stuff that might break everything.../!\ 
	/!\ 										/!\ 
	/!\ Please check the other sites after your install, and rewind the server if 	/!\ 
	/!\ they seem out of order! 							/!\ 
	/!\ 										/!\ 																			/!\ 
	/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ 

	To use GIT, you can use "sourcetree", a windows git client installed on this machine.¨
	The git repository for the galleriet framework is:
		https://mujakob@bitbucket.org/mujakob/gallerietserver.git
	
1.) GIT
	In order to have a controllable environment, we exclusively use git to move any apps onto this server.
	new app repositories can be cloned into C:\DJANGO\galleriet\
	
	copy/paste not allowed. 
	if you don't know what git is, or how to use it:
	https://www.atlassian.com/git/tutorials/what-is-git
	
	place your app into C:\DJANGO\galleriet\
		--- file explorer ----------------------------------------------------------
			C:\DJANGO\galleriet\
			|- other apps\
		-->	|- YOUR_APP\
			|- galleriet\
			|- media\
			|- sed\
			|- .gitignore
			|- db.sqlite3
			|- manage.py
			|- README.txt
			 - web.config

		----------------------------------------------------------------------------
	To clone a repository, you can use "sourcetree", a windows git client installed on this machine.¨
	The git repository for the galleriet framework is:
		https://mujakob@bitbucket.org/mujakob/gallerietserver.git	


2.) configuring the server for your new app
	after cloning your repository, you need to tell the server that it exists.
	
	/!\  your static files HAVE TO BE in /static/YOUR_APP_NAME/... otherwise you ruin it for everyone
	/!\  all directories are relative to C:\DJANGO\galleriet\
	/!\  YOUR_APP_NAME is a placeholder in all examples for the name of your app
	/!\  YOUR_URL is a placeholder for your app's url  
		
	2.1 register the url
		in \galleriet\urls.py
		add your url to the array "urlpatterns", you can just copy paste from your own server url.py
		/!\  make sure not to have collisions with existing urls!
		/!\  make sure to import all packages if you need a new one!
		--- \galleriet\urls.py -----------------------------------------------------
			urlpatterns = [
				## your url
				url(r'^YOUR_URL/', include('YOUR_APP_NAME.urls')),
				## alternative version:
				path('YOUR_URL/', include('YOUR_APP_NAME.urls')),
				## other urls registered here
			] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
		----------------------------------------------------------------------------
		
	2.2 register your app
		in \galleriet\settings.py
		add your app to the array "INSTALLED_APPS". 
		As above, you can just copy from your own server's settings.py
		--- \galleriet\settings.py -------------------------------------------------
			INSTALLED_APPS = [
				'YOUR_APP_NAME.apps.YOUR_APP_NAMEConfig',
				## other apps registered here
				]
		----------------------------------------------------------------------------
		
	2.3 migrate your database
		open a shell in the root directory
		type the command 
		--- cmd --------------------------------------------------------------------
			python manage.py makemigrations YOUR_APP_NAME
		----------------------------------------------------------------------------
		/!\  if any errors occure, read the error messages carefully and fix them!
		then run the command 
		--- cmd --------------------------------------------------------------------
			python manage.py migrate
		----------------------------------------------------------------------------
		/!\  depending on how you have set up your project only "migrate" might suffice, but let's be safe here...
		
	2.4 copy your staticfiles
		to copy all the staticfiles you have used in your project, run the command
		/!\  your static files HAVE TO BE in /static/YOUR_APP_NAME/... otherwise you ruin it for everyone
		--- cmd --------------------------------------------------------------------
			python manage.py collectstatic
		----------------------------------------------------------------------------
			
	2.5 check other apps
		Please make sure that all the other apps still work fine before finishing your job!
		If not, reset the branch to the backup you did above
		
	You should be ready to go!

		
		