from django.db import models
#from django import datetime
from next_prev import next_in_order, prev_in_order

class researcher(models.Model):
    name = models.CharField(max_length=200)
    #role = models.ForeignKey('researcherRoles', on_delete=models.PROTECT) #use dropdown?
   
    profileImage = models.ImageField("Profile picture", upload_to='sed', default='sed/default/avancez.png')
    researchImage = models.ImageField("Image about research field", upload_to='sed', default='sed/default/projectBanner.png')
    bannerProfessional = models.ImageField("Banner in profesional environment", upload_to='sed', default='sed/default/ChalmersPhDthesis.jpg')
    imageTextProfessional = models.CharField("Caption for profesionall banner", max_length=100, blank=True) 
    bannerCasual = models.ImageField("Banner in private context", upload_to='sed', blank=True)
    imageTextCasual = models.CharField("Caption for private banner", max_length=100, blank=True) 
    emailAdress = models.EmailField("Email address") #find email field
    #phoneNumber = forms.RegexField(regex=r'^\+?1?\d{9,15}$', 
    #                            error_message = ("Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."), blank=True)
    phoneNumber = models.CharField("Phone number", max_length=20, blank=True)
    
    persInterest1 = models.CharField("Personal interest", max_length=100, blank=True) #list field?
    persInterest2 = models.CharField("Personal interest", max_length=100, blank=True)
    persInterest3 = models.CharField("Personal interest", max_length=100, blank=True)
    profInterest1 = models.CharField("Research interest", max_length=100, blank=True) #list field?
    profInterest2 = models.CharField("Research interest", max_length=100, blank=True)
    profInterest3 = models.CharField("Research interest", max_length=100, blank=True)
    
    persDescription = models.TextField("Personal description")
    #startDate = models.DateField(default=datetime.date.today)
    
    # role selector field:
    LEADER = 'AA'
    SENIOR = 'BB'
    STUDENT = 'CC'
    GUEST = 'DD'
    ROLE_CHOICES = (
        (LEADER, 'Group Leader'),
        (SENIOR, 'Senior Researcher'),
        (STUDENT, 'PhD Student'),
        (GUEST, 'Guest Researcher'),
    )
    roleX = models.CharField(
        "Role in group",
        max_length=2,
        choices=ROLE_CHOICES,
        default=STUDENT,
    )
    def nextR(self):    
        nextR = next_in_order(self, loop=True)
        return nextR
    def prevR(self):
        prevR = prev_in_order(self, loop=True)
        return prevR
        
    def __str__(self):
        return self.name
        
    def contactCard(self):
        cc = {'name': self.name, 'phone_number':self.phoneNumber, 'email':self.emailAdress, 'company':'Chalmers University of Technology', 'url':'',}
        return cc
    def contactQR(self):
        contact="N:{0};EMAIL:{1};TEL:{2};ADR:Department of Industrial and Materials Science, Chalmers University of Technology".format(self.name, self.emailAdress, self.phoneNumber)
        qr = "https://api.qrserver.com/v1/create-qr-code/?data=MECARD:{}&size=120x120&bgcolor=f6f6f6".format(contact)
        return qr
        
    class Meta:
        ordering = ['roleX', 'name', 'pk']
    #class Meta:
    #    order_with_respect_to = 'role'
            
class partners(models.Model):
    name= models.CharField(max_length=100)
    logo = models.ImageField(upload_to='sed', blank=True)
    type = models.CharField(
        max_length=2,
        choices= (('A', 'Academic'), ('I', 'Industrial')),
        default ='A',
        )        
    def __str__(self):
         return self.name
    class Meta:
        ordering = ['type', 'name', 'pk']
        
class project(models.Model):
    name=models.CharField(max_length=200)
    project_logo = models.ImageField(upload_to='sed/projects', default='/sed/default/projectLogo.png')
    project_illustration = models.ImageField(upload_to='sed/projects', default='/sed/default/projectBanner.png')
    project_lead = models.ForeignKey('researcher', on_delete=models.PROTECT, related_name='lead', null=True)
    description = models.TextField('Project description')
    researchers = models.ManyToManyField(researcher)
    partners = models.ManyToManyField(partners, blank=True)
    
    def __str__(self):
        return self.name

            
# class researcherRoles(models.Model):
        # roleName=models.CharField(max_length=200)
        # orderBy = models.IntegerField()
        # def __str__(self):
            # return self.roleName
        # class Meta:
            # ordering = ['orderBy']