from django.contrib import admin
from .models import researcher, project, partners#, researcherRoles

admin.site.register(researcher)
admin.site.register(project)
admin.site.register(partners)
#admin.site.register(researcherRoles)

# Register your models here.
