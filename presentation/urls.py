from django.conf.urls import url
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views

app_name='presentation'

urlpatterns = [
    #ex: /sed/
    url(r'^$', views.indexView.as_view(), name='index'),
    #ex: /sed/2/
    #url(r'^m/(?P<pk>[0-9]+)/$', views.memberView.as_view(), name='member'),
    path('m/<int:pk>/', views.memberView.as_view(), name='member'),
    path('p/<int:pk>/', views.projectView.as_view(), name='project'),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)