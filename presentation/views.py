from django.shortcuts import render

from .models import researcher
from .models import project
from django.views import generic

class indexView(generic.TemplateView):
    template_name = 'presentation/overview.html'
    #context_object_name = 'all_researchers'
    #model = researcher 
    
    def get_context_data(self, **kwargs):
        context = super(indexView, self).get_context_data(**kwargs)
        context['all_researchers'] = researcher.objects.all()
        context['all_projects'] = project.objects.all()
        return context
        # categories = ['leader', 'senior', 'student', 'guest']
        # for c in categories:
           # try:
               # context[c] = researcher.objects.filter(role__roleName__icontains=c)
           # except researcher.DoesNotExist: 
                # context[c] = None        
    
class memberView(generic.DetailView):
    model = researcher
    template_name = 'presentation/researcher.html'    
    
class projectView(generic.DetailView):
    model = project
    template_name = 'presentation/project.html'
    
class slideView(generic.ListView):
    template_name = 'presentation/slideshow.html'
    context_object_name = 'all_researchers'
    model = researcher